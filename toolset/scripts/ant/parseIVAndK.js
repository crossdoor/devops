(function(cmdOutput, kProperty, ivProperty) {
	function fail(message) {
		var fail = project.createTask("fail");
		fail.setMessage(message);
		fail.execute();
	}
	
	if (cmdOutput == null) {
		fail('No openssl output detected!');
	}

	var split = cmdOutput.split('\n');
	
	if (split.length < 2) {
		fail('Openssl output not formatted as expected!');
	}

	var kLine = split[0].split('=');
	var ivLine = split[1].split('=');

	if (ivLine.length != 2 || kLine.length != 2) {
		fail('Openssl iv/k lines not formatted as expected!');
	}

	project.setProperty(kProperty, kLine[1]);
	project.setProperty(ivProperty, ivLine[1]);
}(attributes.get("cmdoutput"), attributes.get("kproperty"), attributes.get("ivproperty")));